const { urlencoded } = require("express");
const express = require("express");
const mongoose = require("mongoose");

const port = 4000;

const app = express();

mongoose.connect("mongodb+srv://admin:admin@batch245-vega.ien0lsm.mongodb.net/s35-discussion?retryWrites=true&w=majority",{
    useNewUrlParser : true,
    useUnifiedTopology: true
});
let db = mongoose.connection;

db.on("error", console.error.bind(console,"Connection error"));
db.once("open", () =>{ 
    console.log("We are connected to the cloud database!");
})
// model
    const taskSchema = mongoose.Schema({
        name:{
            type:String,
            required:[
                true,"Task name is required!"
            ]
        },
        status:{
            type:String,
            default: "Pending"
        }
    });

    const registerSchema = mongoose.Schema({
        username:{
            type:String,
            required:[
                true,"Username is required!"
            ]
        },
        password:{
            type:String,
            require:[
                true,"Password is required!"
            ]
        }
    });


    const task = mongoose.model("Task",taskSchema);
    const register = mongoose.model("Register",registerSchema);
// middleware
    app.use(express.json());
    app.use(express(urlencoded({extended:true})));
// routing
    app.post("/tasks",(request,response)=>{
        let input = request.body;
        console.log(input.status)
        if(input.status === undefined){
            task.findOne({name:input.name},(error,result) =>{
                console.log(result);
                if(result !== null){
                    return response.send("Task is already existing");
                }
                else{
                    let newTask = new task({
                        name : input.name
                    })
                    newTask.save((saveError,savedTask)=>{
                        if(saveError){
                            return console.log(saveError);
                        }
                        else{
                            return response.send("New Task created!");
                        }
                    });
                }
            });
        }
        else{
            task.findOne({name:input.name},(error,result) =>{
                console.log(result);
                if(result !== null){
                    return response.send("Task is already existing");
                }
                else{
                    let newTask = new task({
                        name : input.name,
                        status: input.status
                    })
                    newTask.save((saveError,savedTask)=>{
                        if(saveError){
                            return console.log(saveError);
                        }
                        else{
                            return response.send("New Task created!");
                        }
                    });
                }
            });
        }
    });

    app.get("/tasks",(request,response)=>{
        task.find({},(error,result)=>{
            if(error){
                console.log(error);
            }
            else{
                return response.send(result);
            }
        });
    })
// Activity
    app.post("/register",(request,response)=>{
        let input = request.body;
        if(input.password === undefined){
            register.findOne({name:input.username},(error,result) =>{
                console.log(result);
                if(result !== null){
                    return response.send("User is already existing");
                }
                else{
                    let newUser = new register({
                        username : input.username
                    })
                    newUser.save((saveError,savedUser)=>{
                        if(saveError){
                            return console.log(saveError);
                        }
                        else{
                            return response.send("New User created!");
                        }
                    });
                }
            });
        }
        else{
            register.findOne({name:input.username},(error,result) =>{
                console.log(result);
                if(result !== null){
                    return response.send("User is already existing");
                }
                else{
                    let newUser = new register({
                        username : input.username,
                        password: input.password
                    })
                    newUser.save((saveError,savedUser)=>{
                        if(saveError){
                            return console.log(saveError);
                        }
                        else{
                            return response.send("New User created!");
                        }
                    });
                }
            });
        }
    });
    app.get("/register",(request,response)=>{
        register.find({},(error,result)=>{
            if(error){
                console.log(error);
            }
            else{
                return response.send(result);
            }
        });
    })

app.listen(port,()=> console.log(`server is running at port: ${port}`));